'use client'
import dayjs from 'dayjs';
import React, { FormEvent, useEffect, useState } from 'react'
import MenuBookIcon from '@mui/icons-material/MenuBook';
import Modal from './Modal';
import LabelTextareaWhite from './elements/LabelTextareaWhite';
import { BabyProfile, Comment } from '../api/type';
import LabelUploadForm from './elements/LabelUploadForm';
import { getCommentsByBabyIdAndDate, postComment } from '../api/commentService';
import CommentCardList from './CommentCardList';
import BasicTimePicker from './BasicTimePIcker';

export interface AddDiaryProps {
    babyProfile: BabyProfile;
    today:string;
}

const AddDiary = ({ babyProfile,today }: AddDiaryProps) => {
    const [comments,setComments] = useState<Comment[]>([]);
    const [selectedTime, setSelectedTime] = useState<any>(dayjs()as any);
    const [diary, setDiary] = useState<Comment>({
        createdAt:selectedTime?.format('YYYY-MM-DDTHH:mm:ssZ'), 
        comment: "", imgUrl: "", babyProfile: babyProfile
    })
    const [isOpen, setIsOpen] = useState<boolean>(false);
    
    useEffect(()=>{
        getComments();
    },[babyProfile, today])

    const getComments = async ()=>{
        try{
            setComments(await getCommentsByBabyIdAndDate(babyProfile.id as number,today) as Comment[])
        }catch(e:any){
            setComments([]);
        }
    }

    const handleChage = (event: any) => {
        setDiary({
            ...diary,
            [event.target.name]: event.target.value
        });
    }
    const handleTimeChange = (newTime:Date|null ) => {
        setSelectedTime(newTime) 
    };
    const handleFile = (event: any) => {
        const fileReader = new FileReader();
        fileReader.onload = () => {
            setDiary({
                ...diary,
                imgUrl: fileReader.result as string
            })
        }
        fileReader.readAsDataURL(event.target.files[0])
    }
    const handleSubmit = async (event: FormEvent) => {
        event.preventDefault();
        try{
            const createdTime = dayjs(selectedTime).utc().format('THH:mm:ssZ');  
            const createdDatetime = `${today}${createdTime}`;
            const persisted = await postComment({...diary, createdAt:createdDatetime});
            const newComments = [...comments, persisted];
            const sortedData = newComments.sort((a, b) => {
            const timeA = new Date(a.createdAt as string);
            const timeB = new Date(b.createdAt as string);
            if (timeA < timeB) {
                return -1;
            }
            if (timeA > timeB) {
                return 1;
            }
            return 0;
        });
            setComments(sortedData);
            setDiary({
                createdAt: "", comment: "", imgUrl: "", babyProfile: babyProfile
            })
            setIsOpen(false);
        }catch(error:any){
            console.log(error)
        }
    }
    return (
    <div className="md:mt-8">
        <div className="flex flex-col justify-center">
            <div className="flex justify-center">
            <button onClick={() => { setIsOpen(true) }} className=" flex w-64 px-8 py-4 text-white bg-blue-500 rounded transform hover:bg-blue-400 hover:scale-95 duration-200">
                <div className="mr-4"> Add Photo diary</div>
                <div className="flex gap-2">
                    <MenuBookIcon />
                </div>
            </button>
            <Modal isOpen={isOpen} onClose={() => setIsOpen(false)}>
                <form onSubmit={handleSubmit} className="w-80 flex flex-col items-center justify-center ">
                    <h2 className="font-bold my-2">Record today's events</h2>
                    <BasicTimePicker selectedTime={selectedTime} onTimeChange={handleTimeChange} />
                    <LabelTextareaWhite label="Your comment" onChange={handleChage} placeholder="" name="comment" />
                    <LabelUploadForm label="upload photo" name="imgUrl" onChange={handleFile} />
                    <button className="shadow bg-red-500 hover:bg-red-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded mb-4">Add Record</button>
                </form>
            </Modal>
            </div>
        </div>
        <CommentCardList comments={comments?comments:null} isModifiable={true}/>
    </div>
    )
}

export default AddDiary