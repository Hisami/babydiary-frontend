import React, { useState } from 'react';

interface Props {
    text:string;
    maxLength:number;
}

const ExpandableText = ({ text, maxLength }:Props) => {
    const [expanded, setExpanded] = useState(false);

    const toggleExpand = () => {
        setExpanded(!expanded);
    };

    return (
        <div>
            {expanded ? (
                <p>{text}<span onClick={toggleExpand} className="text-blue-500 cursor-pointer">Fermer</span></p>
            ) : (
                <p>{text.slice(0, maxLength)}{text.length > maxLength && '...'} <span onClick={toggleExpand} className="text-blue-500 cursor-pointer">Lire la suite</span></p>
            )}
        </div>
    );
};

export default ExpandableText;
