import React from 'react';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { TimePicker } from '@mui/x-date-pickers/TimePicker';
import 'dayjs/locale/fr';


interface BasicTimePickerProps {
    selectedTime: Date|null;
    onTimeChange: (newTime: Date | null) => void;
}

export default function BasicTimePicker({ selectedTime, onTimeChange }: BasicTimePickerProps) {
    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}  >
            <DemoContainer components={['TimePicker']}>
                <TimePicker　　
                    label="Quand?"
                    value={selectedTime as Date}
                    onChange={onTimeChange}
                />
            </DemoContainer>
        </LocalizationProvider>
    );
}
