import React from 'react';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import "dayjs/locale/fr";

interface BasicDatePickerProps {
    selectedDate: string | null;
    onDateChange: any;
}

export default function BasicDatePicker({ selectedDate, onDateChange }: BasicDatePickerProps) {
    return (
        <LocalizationProvider  dateAdapter={AdapterDayjs} >
            <DemoContainer components={['DatePicker']}>
                <DatePicker
                    label="Quand date?"
                    value={selectedDate}
                    onChange={onDateChange}
                />
            </DemoContainer>
        </LocalizationProvider>
    );
}
