"use client"

import React, { FormEvent, useContext, useEffect, useState } from 'react'
import { AuthContext } from '../api/auth/AuthContextProvider';
import { editUserProfile } from '../api/auth/authService';
import { User } from '../api/type';
import { avatarimages } from '../data/avatarimages';
import AvatarButton from './elements/AvatarButton';
import Button from './elements/Button';
import LabelForm from './elements/LabelForm';
import Modal from './Modal';
interface UserProfileProps {
    user?: User;
    onUserUpdate: any
}

const UserProfile = ({ user, onUserUpdate }: UserProfileProps) => {
    const [isEditing, setIsEditing] = useState<boolean>(false);
    const [userItem, setUserItem] = useState<User | undefined>(user);
    useEffect(() => {
        if (user) {
            setUserItem(user)
        }
    }, [])
    const onClick = () => {
        setIsEditing(true)
    }
    const closeModal = () => {
        setIsEditing(false);
    }
    const handleChange = (event: any) => {
        setUserItem({
            ...userItem,
            [event.target.name]: event.target.value
        } as User);
    }
    const handleSubmit = async (event: FormEvent) => {
        event.preventDefault();
        try {
            const persisted = await editUserProfile(userItem as User);
            onUserUpdate(persisted);
            setIsEditing(false);
            console.log(persisted)
        } catch (e) {
            console.log(e)
        }
    }

    return (
        <>
            <h2 className="bg border-blue py-1 px-4  text-blue mt-4 ">User info</h2>
            <div className="flex flex-col justify-center items-center mt-4 bg-white w-64 md:w-96 p-4 rounded-lg">
                <img src={user?.imgUrl} className="inline-block h-16 md:h-32 w-16 md:w-32 rounded-full  bg-white  " alt="userprofile"></img>
                <h3 className="my-2">Username:{user?.name}</h3>
                <button onClick={onClick} className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-normal py-2 px-4 rounded">Edit user Profile</button>
            </div>
            {isEditing && (
                <Modal isOpen={true} onClose={closeModal} >
                    <div className="mx-8">
                        <div className="flex justify-center m-4">
                            <p className="font-bold">Edit Profile</p>
                        </div>
                        <form className="w-full max-w-sm" >
                            <LabelForm label="Name" onChange={handleChange} name="name" placeholder={user?.name} type="string" />
                            <div>
                                <div className="flex justify-center">
                                    <p className="text-gray-500 font-bold">Select your avatar</p>
                                </div>
                                <div className="w-full">
                                    <div className="md:flex md:items-center overflow-x-scroll">
                                        {avatarimages.map((avatardata) => (
                                            <AvatarButton
                                                onClick={() => { setUserItem({ ...userItem, imgUrl: avatardata.imgUrl } as User) }}
                                                key={avatardata.id}
                                                imgUrl={avatardata.imgUrl}
                                                alt={avatardata.alt}
                                            />
                                        ))}
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                            <div className="mt-4 flex justify-center">
                                <Button onClick={handleSubmit} text="Submit" />
                            </div>
                        </form>
                    </div>
                </Modal>
            )}

        </>
    );
};


export default UserProfile