'use client'
import { useEffect,useState } from 'react';
import { deleteComment, editComment } from '../api/commentService';
import { Comment } from '../api/type';
import CommentCard from './elements/CommentCard';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation  } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/navigation'

interface CommentCardListProps {
    comments: Comment[] | null;
    isModifiable:boolean;
}

const CommentCardList = ({ comments,isModifiable }: CommentCardListProps) => {
    const [commentsModified, setCommentsModified] = useState<Comment[]>();
    useEffect(() => {
        setCommentsModified(comments as Comment[])
    }, [comments])

    const handleModifyComment = async (comment: Comment) => {
        const updatedComment = await editComment(comment);
        const updatedComments = commentsModified?.map((c) =>
            c.id == updatedComment.id ? updatedComment : c);
        setCommentsModified(updatedComments);
    }
    const deleteCom = async (comment: Comment) => {
        await deleteComment(comment);
        setCommentsModified(commentsModified?.filter(item => item.id != comment.id));
    }
    return (
        <div>
            <div className=" md:mx-64">
            <Swiper
                modules={[Navigation]}
                breakpoints={{
                    1024: {
                        slidesPerView: 2,
                        spaceBetweenSlides: 50
                    },
                    768: {
                        slidesPerView: 1,
                        spaceBetweenSlides: 20
                    }
                }}        
                pagination={{ clickable: true }}
                scrollbar={{ draggable: true }}
                navigation
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}>
                {commentsModified?.map(comment => (
                    <SwiperSlide key={comment.id}>
                        <CommentCard comment={comment} handleModifyComment={handleModifyComment} deleteComment={() => { deleteCom(comment) }}
                        isModifiable={isModifiable} />
                    </SwiperSlide>
                ))}
            </Swiper> 
        <div>
    </div>
    </div>
</div>
    )
}

export default CommentCardList