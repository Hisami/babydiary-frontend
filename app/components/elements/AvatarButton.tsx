'use client'
import React, { useState } from 'react'

export interface AvatarButtonProps {
    onClick:any;
    imgUrl:string;
    alt:string;
    selected?:boolean;
}

const AvatarButton = ({onClick,imgUrl,alt,selected}: AvatarButtonProps) => {
    return (
        <button type="button"onClick={onClick} className="relative bg-transparent border-none cursor-pointer p-2 text-3xl hover:bg-neutral-300 hover:text-4xl focus:bg-neutral-500 
        ">
            <img
            className="inline-block h-10 w-10 md:h-16 md:w-16 rounded-full ring-2 ring-white "
            src={imgUrl}
            alt={alt}
            />   
        {selected &&<div className="bg-orange-500 rounded-full w-3 h-3 absolute bottom-0 right-0"></div> } 
        </button>
    )
}

export default AvatarButton