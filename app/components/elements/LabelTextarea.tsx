import React from 'react'
import Label from './Label';

export interface FormProps {
    label:string;
    onChange: any;
    name: string;
    placeholder?:string;
    type?:string;
}

const LabelTextarea = ({ label, onChange, name, placeholder }: FormProps) => {
    return (
        <div className="md:flex md:items-center mb-6">
                <div className="md:w-1/3">
                    <Label label={label}/>
                </div>
                <div className="md:w-2/3">
                    <textarea className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500" id="inline-full-name"  onChange={onChange} name={name} placeholder={placeholder} />
                </div>
        </div>
    )
}

export default LabelTextarea