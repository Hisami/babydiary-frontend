
import { EntryCount } from '@/app/api/type'
import React from 'react'


interface ButtonListProps {
    entryCount:EntryCount
}

const IconCount = ({ entryCount }:ButtonListProps) => {

    return (
    <div className="flex">
        <span>{entryCount?.icon_name}</span>
        <span>x</span>
        <span>{entryCount?.entryCount}</span>
    </div>  
    )
}

export default IconCount