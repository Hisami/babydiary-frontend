import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import { Entry } from '@/app/api/type'
import React from 'react'
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

dayjs.extend(utc);
dayjs.extend(timezone);

export interface EntryItemProps {
    entry: Entry;
    onClick:any;
    deleteEntry:any;
}

const EntryItem = ({ entry,onClick,deleteEntry }: EntryItemProps) => {
    const parisTime = dayjs(entry.createdAt as string);
    const formattedTime = parisTime.format('HH:mm');
    return (
    <div className="w-64 md:w-96">
        <div className="flex flex-col mb-2 ">
            <div className="flex border-l-4 border-blue-500 justify-between md:text-lg ">
                <div className="flex">
                <p className="mr-2 ">{formattedTime}</p>
                <p><span>{entry?.icon.name}</span>{entry?.icon.explication}</p>
                </div>
                <div>
                <button onClick={onClick}>
                <EditIcon color="primary" fontSize="small" />
                </button>
                <button onClick={deleteEntry}>
                <DeleteIcon  fontSize="small" />
                </button>
                </div>
            </div>
            <div className="flex justify-start border">
                <p className="">{entry?.description}</p>
            </div>
        </div>
    </div>
    )
}

export default EntryItem