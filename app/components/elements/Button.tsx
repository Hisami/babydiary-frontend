import React from 'react'

export interface ButtonProps{
    onClick?:any;
    text:string
}

const Button = ({onClick,text}:ButtonProps) => {
  return (
    <button type="button" onClick={onClick} className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded" >
    {text}
    </button>
  )
}

export default Button