'use client'
import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';
import { Comment } from '@/app/api/type';
import React, { useEffect, useRef, useState } from 'react'
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

dayjs.extend(utc);
dayjs.extend(timezone);

export interface CommentCardProps {
    comment: Comment;
    handleModifyComment: (editedComment:Comment)=>void;
    deleteComment: any;
    isModifiable?:boolean
}

const CommentCard = ({ comment, handleModifyComment, deleteComment,isModifiable}: CommentCardProps) => {
    const ref = useRef<HTMLInputElement>(null);
    const [isEditing, setIsEditing] = useState(false);
    const [editedComment, setEditedComment] = useState(comment)
    const time = dayjs(comment.createdAt as string);
    const formattedTime = time.format('HH:mm');
    useEffect(()=>{
        if(isEditing){
            ref.current?.focus();
        }
    },[isEditing])

    const makeLink = (link: string) => {
        if (link.startsWith('http')) {
            return link;
        }
        return process.env.NEXT_PUBLIC_SERVER_URL + '/uploads/' + link;
    }
    const handleSave =()=>{
        handleModifyComment(editedComment)
        setIsEditing(false);
    } 

    return (
            <div className="md:w-240px rounded shadow-lg m-4">
                <img className="w-full" src={makeLink(comment.imgUrl as string)}  />
                <div className="px-6 py-4 bg-white">
                    <div className="text-sm mb-2 flex justify-between items-center">
                    <p className="mr-2 ">{formattedTime}</p>
                {isModifiable?( <div className="flex">
                    <button onClick={() => setIsEditing(true)} className="mr-2">
                        <EditIcon color="primary" fontSize="small" />
                    </button>
                    <button onClick={deleteComment}>
                        <DeleteIcon fontSize="small" />
                    </button>
                </div>):null}
            </div>
                    {isEditing ? (
                        <>
                        <input
                            type="text"
                            className='mr-2 py-1 px-2 rounded border-gray-400  border'
                            value={editedComment.comment}
                            onChange={(e: React.ChangeEvent<HTMLInputElement>) => { setEditedComment({...editedComment,comment:e.target.value}) }} />
                            <button onClick={handleSave} className="  px-2 border-blue-500 hover:bg-blue-500 hover:text-white border text-blue-500 ml-3 rounded-lg">save</button>
                        </>
                    ) : (
                        <p className="text-gray-700 text-base">
                            {comment.comment}
                        </p>
                    )}
                </div>
            </div>
    )
}

export default CommentCard;