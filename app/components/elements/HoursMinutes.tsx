import React from 'react'

export interface HoursMinutesProps {
    datetime: string;
}
const HoursMinutes = ({ datetime }: HoursMinutesProps) => {
    const dateTime = new Date(datetime as string);
    const hours = dateTime.getUTCHours();
    const formattedHours = String(hours).padStart(2,'0');
    const minutes = dateTime.getUTCMinutes();
    const formattedMinutes = String(minutes).padStart(2, '0'); 
    return (
        <span>{formattedHours}:{formattedMinutes}</span>
    )
}

export default HoursMinutes