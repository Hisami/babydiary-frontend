import React from 'react'

export interface FormProps {
    label:string;
}

const Label = ({ label }: FormProps) => {
    return (
        <label className="block text-gray-500 font-bold md:text-right mb-1 md:mb-0 pr-4" >
        {label}
        </label>
            
    )
}

export default Label