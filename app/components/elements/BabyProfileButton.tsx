'use client'
import Link from "next/link"
interface BabyProfileButtonProps {
    onClick?: () => void
}

const BabyProfileButton = ({ onClick }: BabyProfileButtonProps) => {
    return (
        <Link href={'/user/babyprofile'}>
        <button onClick={onClick}className="w-30 px-4 py-2 text-white bg-blue-500 rounded transform hover:bg-blue-400 hover:scale-95 duration-200">Créer un baby profile</button>
        </Link>
    )
}

export default BabyProfileButton