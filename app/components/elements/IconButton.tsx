
import { Icon } from "@/app/api/type"
interface IconButtonProps {
    icon:Icon,
    onClick: () => void
}

const IconButton = ({ icon, onClick }: IconButtonProps) => {
    return (
    <button 
        className="md:w-24 bg-white border border-blue mb-2 cursor-pointer md:p-2 text-xl md:text-3xl hover:text-2xl active:bg-neutral-400 md:hover:text-4xl md:text-4xl"
        onClick={onClick}>
            <div className="flex flex-col items-center  rounded-lg ">
                <span role="img" aria-label={icon.explication} >{icon.name}</span>
                <span className="md:text-sm hidden md:block ">{icon.explication}</span>      
            </div>
    </button>
    )
}

export default IconButton