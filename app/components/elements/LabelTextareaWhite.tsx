import React from 'react'

export interface FormProps {
    label:string;
    onChange: any;
    name: string;
    placeholder?:string;
    type?:string;
}

const LabelTextareaWhite = ({ label, onChange, name, placeholder,type }: FormProps) => {
    return (
        <div className="mb-2">
                <div className="">
                    <label className="block text-gray-500 font-normal  mb-1 pr-4" >
                        {label}
                    </label>
                </div>
                <div className="">
                    <textarea className="bg-white-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-blue-500" id="inline-full-name" onChange={onChange} name={name} placeholder={placeholder} />
                </div>
        </div>
    )
}

export default LabelTextareaWhite