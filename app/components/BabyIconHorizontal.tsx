
import dayjs from 'dayjs';
import React from 'react'
import { BabyProfile } from '../api/type'
import BabyProfileButton from './elements/BabyProfileButton';

export interface babyIconProps {
    babyProfile?:BabyProfile;
    date:string;
}

const BabyIconHorizontal =({babyProfile,date}:babyIconProps) => {
const dataNow = dayjs(date);
const babyBirthDay = dayjs(babyProfile?.birthDate as string);
const yearsDiff = dataNow.year() - babyBirthDay.year();
const monthsDiff = dataNow.month() - babyBirthDay.month();
const totalMonthsDiff = yearsDiff * 12 + monthsDiff;
    return (
        <div>
            {babyProfile?(
                <div className="flex  justify-center items-center mt-4 gap-4" >
                <div className='flex flex-col justify-center items-center'>
                <h3 className="font-semibold text-blue-800" >{babyProfile?.name}</h3>
                <img src={babyProfile?.imgUrl} className="inline-block h-16 md:h-32 w-16 md:w-32 rounded-full ring-2 ring-cyan bg-white " alt=""></img>
                </div>
                <div className='flex flex-col justify-center items-center'>
                <p className="text-sm md:text-sm">
                    <span className="font-semibold">{totalMonthsDiff}</span>
                    <span> months</span> 
                </p>
                </div>  
            </div>
            ):(
            <div className="flex flex-col justify-center items-center m-4">
                <p>No Baby Profile</p>
                <BabyProfileButton/>
            </div> 
            ) }     
        </div>
    )
}

export default BabyIconHorizontal