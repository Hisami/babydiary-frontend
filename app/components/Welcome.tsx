"use client"
import Link from 'next/link';
import React, { useState } from 'react'


const Welcome = () => {
    const [heart,setHeart]=useState<boolean>(false);

    return (
    <div className="flex flex-col items-center justify-center min-h-screen">
        {heart && <img className="w-12 md:w-24 rotate-45 absolute top-50 md:top-100 right-20 md:right-50"
        src="https://4.bp.blogspot.com/-XWP8gzXWJXw/UP5whVRKQ1I/AAAAAAAAK8Q/To50kdanjTQ/s1600/heart_small.png"/>}
        <img src="https://1.bp.blogspot.com/-AJjw6k7Oh8Q/U57C96pZS2I/AAAAAAAAhfc/y18XLb5rKO4/s400/akachan_omutsu.png" className="w-32 md:w-48 "/>
        <Link href={'/login'}>
        <button onClick={()=>setHeart(true)}className="w-30 px-4 py-2 text-white bg-blue-500 rounded transform hover:bg-blue-400 hover:scale-95 duration-200">Login</button>
        </Link>
        <Link href={'/signup'}>
        <button onClick={()=>setHeart(true)}className="mt-4 w-30 px-4 py-2 text-white bg-blue-500 rounded transform hover:bg-blue-400 hover:scale-95 duration-200">Créer un account</button>
        </Link>
    </div> 
    );
};


export default Welcome;