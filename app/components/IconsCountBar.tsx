'use client'
import React, { useEffect, useState } from 'react'
import { EntryCount } from '../api/type';
import IconCount from './elements/IconCount';

interface ButtonListProps {
    entryCounts?: EntryCount[];
}

const IconsCountBar = ({ entryCounts }: ButtonListProps) => {
    const [counts, setCounts] = useState<EntryCount[]>(entryCounts as EntryCount[]);
    useEffect(() => {
        setCounts(entryCounts as EntryCount[]);
    }, [entryCounts])
    return (
        <div className="w-72">
            {counts && counts.length > 0 ? (
                <div className="w-full flex items-center justify-start gap-3  mt-2 h-12">
                    {counts?.map((item) => (
                        <IconCount
                            key={item.id}
                            entryCount={item}
                        />
                    ))}
                </div>
            ) : (<div className="mt-4"></div>)}

        </div>

    )
}

export default IconsCountBar