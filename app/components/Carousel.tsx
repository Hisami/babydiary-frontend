'use client'
import React, { useEffect, useState } from 'react';
import { deleteComment, editComment } from '../api/commentService';
import CommentCard from './elements/CommentCard';
import { Comment } from '../api/type';


export interface CarouselProps {
    comments: Comment[]|null;
}

const CommentCarousel = ({ comments }: CarouselProps) => {
    const [commentsModified,setCommentsModified]=useState<Comment[]>();
    useEffect(()=>{
        setCommentsModified(comments as Comment[])
    },[comments])
    
    const handleModifyComment = async(comment:Comment)=>{
        const updatedComment = await editComment(comment);
        const updatedComments = commentsModified?.map((c)=>
        c.id==updatedComment.id?updatedComment:c);
        setCommentsModified(updatedComments);
    }
    const deleteCom =async (comment:Comment)=>{
        await deleteComment(comment);
        setCommentsModified(commentsModified?.filter(item => item.id != comment.id));
    }


    return (
        <div id="controls-carousel" className="relative w-full" data-carousel="static">
            <div className="relative w-96 overflow-hidden rounded-lg">
                {commentsModified?.map((comment, index) => (
                    <div key={index} className="duration-700 ease-in-out" data-carousel-item={index === 0 ? 'active' : null}>
                        <CommentCard key={comment.id} comment={comment} handleModifyComment={handleModifyComment} deleteComment={()=>{deleteCom(comment)}} />
                    </div>
                ))}
            </div>
            <button type="button" className="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-prev><span>Previous</span>
            </button>
            <button type="button" className="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none" data-carousel-next><span>Next</span>
            </button>
        </div>
    );
};

export default CommentCarousel;


