"use client"
import dayjs from 'dayjs';
import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react'
import { BabyProfile, Entry, EntryCount, Icon } from '../api/type';
import BasicTimePicker from './BasicTimePIcker';
import IconButton from './elements/IconButton';
import Modal from './Modal';
import LabelTextareaWhite from './elements/LabelTextareaWhite';
import { getEntryByBabyIdAndDate, getEntryCountByBabyIdAndDate, postEntry } from '../api/entryService';
import EntryList from './EntryList';
import IconsCountBar from './IconsCountBar';


interface ButtonListProps {
    icons: Icon[]|undefined;
    babyProfile:BabyProfile;
    today:string
}

const IconList =  ({icons,babyProfile,today}: ButtonListProps) => {
    const [error, setError] = useState('');
    const [entries,setEntries]= useState<Entry[]>([]);
    const [entryCounts,setEntryCounts]=useState<EntryCount[]>([]);
    const [selectedIcon,setSelectedIcon] = useState<Icon|null>(null);
    const [selectedTime, setSelectedTime] = useState<any>(dayjs()as any);
    const [entry, setEntry] = useState<Entry>({
        createdAt:selectedTime?.format('YYYY-MM-DDTHH:mm:ssZ'),description:"", icon:{name:"",explication:""}, babyProfile:babyProfile
    });
    useEffect(()=>{
        getEntries();
    },[])
    const getEntries =async()=>{
        try{
            setEntries( await getEntryByBabyIdAndDate(babyProfile.id as number,today) as Entry[]);
            setEntryCounts( await getEntryCountByBabyIdAndDate(babyProfile.id as number,today) as EntryCount[]);
        }catch(e:any){
            setError(e);
            setEntries([]);
            setEntryCounts([]);
        }   
    }
    const handleTimeChange = (newTime:Date|null ) => {
        setSelectedTime(newTime)
    };
    const onChange =(e:ChangeEvent<HTMLInputElement>)=>{
        setEntry({...entry,description:e.target.value,babyProfile:babyProfile})
    };
    const openModal = (icon:Icon)=>{
        setSelectedIcon(icon);
        setEntry({...entry,icon:icon})
    };
    const closeModal = ()=>{
        setSelectedIcon(null)
    }

    async function handleSubmit(e:FormEvent){
        e.preventDefault();
        try{
            const createdTime = dayjs(selectedTime).utc().format('THH:mm:ssZ');  
            const createdDatetime = `${today}${createdTime}`;
            const persisted = await postEntry({...entry, createdAt: createdDatetime});
            const newEntries = [...entries, persisted];
            const sortedData = newEntries.sort((a, b) => {
            const timeA = new Date(a?.createdAt as string);
            const timeB = new Date(b?.createdAt as string);
            if (timeA < timeB) {
                return -1;
            }
            if (timeA > timeB) {
                return 1;
            }
            return 0;
        });
        setEntries(sortedData as Entry[]);
        setEntryCounts(await getEntryCountByBabyIdAndDate(babyProfile.id as number, today) as EntryCount[]);
        } catch(error:any){
            console.log(error);
        }
        closeModal()
      /*   setSelectedTime(dayjs(new Date(today)) as any); */
    }
    const deleteEntryUpdate = async()=>{
        setEntryCounts( await getEntryCountByBabyIdAndDate(babyProfile.id as number,today) as EntryCount[]);
    }
    return (
        <div className=" flex flex-col justify-center items-center">
            <div className="w-60 flex flex-center">
                <IconsCountBar entryCounts={entryCounts}/>
            </div> 
            <div>
            <EntryList entries={entries} baby={babyProfile} date={today} deleteEntryUpdate={deleteEntryUpdate}/>
                <div className=" w-42 flex justify-center ">
                    <ul className="  space-y-3  md:space-x-8 overflow-x-auto  ">
                    {icons?.map((icon) => (
                        <IconButton
                        key={icon.id}
                        icon={icon} onClick={()=>openModal(icon)}/>
                    ))}
                    {selectedIcon && (
                        <Modal isOpen={true} onClose={closeModal}>
                            <div className="w-64 flex flex-col items-center justify-center justify-center"> 
                                <h2>{selectedIcon.name}{selectedIcon.explication}</h2>
                                <BasicTimePicker selectedTime={selectedTime} onTimeChange={handleTimeChange} />
                                <LabelTextareaWhite label="Add comment" onChange={onChange}  name="Information" placeholder="Add detail" type="text"/>
                                <div className="mb-2">
                                    <button onClick={handleSubmit}className="shadow bg-blue-500 hover:bg-blue-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded">Register</button>
                                </div>
                            </div>           
                        </Modal>
                    )}
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default IconList