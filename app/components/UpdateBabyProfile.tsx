"use client"

import dayjs from 'dayjs';
import React, { FormEvent, useEffect, useState } from 'react'
import { editBabyProfile } from '../api/babyProfileService';
import { BabyProfile, User } from '../api/type';
import { babyavatarimages } from '../data/babyavatarimages';
import BasicDatePicker from './BasicDatePicker';
import AvatarButton from './elements/AvatarButton';
import BabyProfileButton from './elements/BabyProfileButton';
import Button from './elements/Button';
import LabelForm from './elements/LabelForm';
import LabelTextarea from './elements/LabelTextarea';
import Modal from './Modal';
interface UpdateBabyProfileProps {
    user?:User;
    babyProfile?: BabyProfile|undefined;
    onBabyUpdate:any;
}

const UpdateBabyProfile = ({user, babyProfile ,onBabyUpdate}: UpdateBabyProfileProps) => {
    const [isEditing, setIsEditing] = useState<boolean>(false);
    const [babyItem, setBabyItem] = useState<BabyProfile|undefined>(babyProfile as BabyProfile);
    const [selectedDate, setSelectedDate] = useState<any>(dayjs(new Date(babyProfile?.birthDate as string)) as any);

    useEffect(()=>{
        setBabyItem(babyProfile)
    },[babyProfile])

    const onClick = () => {
        setIsEditing(true)
    }
    const closeModal = () => {
        setIsEditing(false);
    }
    const handleChange = (event:any) => {
        setBabyItem({
            ...babyItem,
            [event.target.name]: event.target.value
        } as BabyProfile);
    }

    const handleDateChange = (newDate: Date | null) => {
        setSelectedDate(newDate)
        const createdDatetime = dayjs(newDate).format('YYYY-MM-DD');
        setBabyItem({ ...babyItem, birthDate: createdDatetime } as BabyProfile);
    };
    const handleSubmit = async(event:FormEvent) => {
        event.preventDefault();
        try{
            if(user)
            setBabyItem({...babyItem,user:user})
            const persisted = await editBabyProfile(babyItem as BabyProfile);
            onBabyUpdate(persisted)
            setIsEditing(false);
        }catch(e){
            console.log(e)
        }
    }

    return (
        <>
            <div className="flex flex-col justify-center items-center mt-4">
                <h2 className="bg border-blue py-1 px-4 mb-2 text-blue ">Baby Profile</h2>
                <div className="flex flex-col justify-center items-center mt-4 bg-white w-64 md:w-96 p-4 rounded-lg">
                    {babyProfile ? (
                        <div className="flex flex-col justify-center items-center">
                            <img src={babyProfile?.imgUrl} className="inline-block h-16 md:h-32 w-16 md:w-32 rounded-full  bg-white  " alt="userprofile"></img>
                            <div className="flex flex-col justify-start m-4">
                                <p>name:  {babyProfile.name}</p>
                                <p>date de naissance:  {new Date(babyProfile?.birthDate as string).toLocaleDateString()}</p>
                                <p>introduction:  {babyProfile.description}</p>
                            </div>
                            <button onClick={onClick} className="shadow bg-purple-500 hover:bg-purple-400 focus:shadow-outline focus:outline-none text-white font-normal py-2 px-4 rounded">Edit BabyProfile</button>
                        </div>
                    ) : (<BabyProfileButton />)}
                </div>
            </div>
            {isEditing && babyProfile && (
                <Modal isOpen={true} onClose={closeModal} >
                    <div className="mx-8">
                        <div className="flex justify-center m-4">
                            <p className="font-bold">Edit Baby Profile</p>
                        </div>
                        <form className="w-full max-w-sm" >
                            <LabelForm label="Name" onChange={handleChange} name="name" placeholder={babyProfile?.name} type="string" />
                            <LabelTextarea label="Description" onChange={handleChange} name="description" placeholder={babyProfile?.description} type="string" />
                            <p className="text-gray-500 font-bold">Date de naissance</p>
                            <BasicDatePicker selectedDate={selectedDate} onDateChange={handleDateChange} />
                            <div>
                                <div className="md:flex md:justify-center mt-6">
                                    <p className="text-gray-500 font-bold">Select your avatar</p>
                                </div>
                                <div className="md:flex md:items-center overflow-x-scroll">
                                    {babyavatarimages.map((avatardata) => (
                                        <AvatarButton
                                            onClick={() => { setBabyItem({ ...babyItem, imgUrl: avatardata.imgUrl } as BabyProfile) }}
                                            key={avatardata.id}
                                            imgUrl={avatardata.imgUrl}
                                            alt={avatardata.alt}
                                        />
                                    ))}
                                </div>
                            </div>
                            <div>
                            </div>
                            <div className=" flex justify-center mt-6">
                                <Button onClick={handleSubmit} text="Submit un baby" />
                            </div>
                        </form>
                    </div>
                </Modal>
            )}

        </>
    );
};


export default UpdateBabyProfile