'use client'
import React, {  useEffect, useState } from 'react'
import { BabyProfile } from '../api/type'

export interface babyIconProps {
    babyProfile:BabyProfile;
    date?:string;
}
const BabyIcon = ({babyProfile,date}:babyIconProps) => {      
        const [dateNow,setDataNow] = useState(new Date())
        useEffect(()=>{
            if(date){
                setDataNow(new Date(date))
            }else{
                setDataNow(new Date())
            }
        },[])
        const babyBirthDay = new Date(babyProfile?.birthDate as string);
        const yearsDiff = dateNow.getFullYear()- babyBirthDay.getFullYear();
        const monthsDiff = dateNow.getMonth()- babyBirthDay.getMonth();
        const totalMonthsDiff = yearsDiff*12 + monthsDiff;    
    return (
        <div>
            <div className="flex flex-col justify-center items-center mt-4 " >
                <img src={babyProfile?.imgUrl} className="inline-block h-16 md:h-32 w-16 md:w-32 rounded-full ring-2 ring-cyan bg-white " alt=""></img>
                <h3 className="font-semibold text-blue-800" >{babyProfile?.name}</h3>
                <p className="text-sm md:text-sm">
                    <span className="font-semibold">{totalMonthsDiff}</span>
                    <span> months</span> 
                </p>
            </div>
        </div>
    )
}

export default BabyIcon