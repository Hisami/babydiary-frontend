'use client'
import dayjs from 'dayjs';
import React, { ChangeEvent, useEffect, useState } from 'react'
import { deleteEnt, editEntry, getEntryByBabyIdAndDate } from '../api/entryService';
import { BabyProfile, Entry } from '../api/type';
import BasicTimePicker from './BasicTimePIcker';
import EntryItem from './elements/EntryItem';
import LabelTextareaWhite from './elements/LabelTextareaWhite';
import Modal from './Modal';

interface EntryListProps {
    entries: Entry[];
    baby:BabyProfile;
    date:string;
    deleteEntryUpdate?:any;
}

const EntryList = ({ entries, baby,date,deleteEntryUpdate }: EntryListProps) => {
    const [modifiedEntries, setModifiedEntries] = useState<Entry[]>(entries);
    useEffect(() => {
        setModifiedEntries(entries)
    }, [entries])
    const [selectedEntry, setSelectedEntry] = useState<Entry | null>(null);
    const handleTimeChange = (newTime: Date | null) => {
        if (newTime) {
            const createdTime = dayjs(newTime).utc().format('THH:mm:ssZ'); 
            const createdDatetime = `${date}${createdTime}`;
            setSelectedEntry({ ...selectedEntry, createdAt: createdDatetime } as Entry);
            console.log(createdDatetime)
        }
    };
    const onChange = (e: ChangeEvent<HTMLInputElement>) => {
        setSelectedEntry({ ...selectedEntry, description: e.target.value } as Entry)
    };
    const openModal = (entry: Entry) => {
        setSelectedEntry(entry);
    };
    const closeModal = () => {
        setSelectedEntry(null);
    }
    const updateEntry = async (entry: Entry) => {
        const updatedEntry = await editEntry(entry);
        const newEntries = entries.map((e) => e.id == updatedEntry.id ? updatedEntry : e);
        const sortedData = newEntries.sort((a, b) => {
            const timeA = new Date(a.createdAt as string);
            const timeB = new Date(b.createdAt as string);
            if (timeA < timeB) {
                return -1;
            }
            if (timeA > timeB) {
                return 1;
            }
            return 0;
        });
        setModifiedEntries(sortedData);
        setSelectedEntry(null);
    }
    const deleteEntry = async (entry: Entry) => {
        await deleteEnt(entry)
        //setModifiedEntries(entries.filter(item => item.id != entry.id));
        setModifiedEntries(await getEntryByBabyIdAndDate(baby.id as number,date) as Entry[])
        deleteEntryUpdate()
        setSelectedEntry(null);
    }
    return (
        <>
        <div className=" md:mx-0  bg-white rounded-md border w-800">
            <div className="h-12 bg-orange-400 px-4 flex items-center justify-center text-white">Today's Records</div>
            <div className="px-8 py-6 h-64 md:h-88 overflow-scroll">
            {modifiedEntries && modifiedEntries.length >0?(
                <>
                {modifiedEntries?.map((entry) => (
                    <EntryItem key={entry.id}
                        entry={entry}
                        onClick={() => openModal(entry)}
                        deleteEntry={() => deleteEntry(entry)}
                    />
                ))}
                </>):(<p>No entries at the moment. Click the icon button to add time and comments.</p>)}                
            </div>
        </div>
            {selectedEntry && (
                <Modal isOpen={true} onClose={closeModal}>
                    <div className="w-64 flex flex-col items-center justify-center ">
                        <h2>Modify Entry</h2>
                        <h3>{selectedEntry.icon.name}{selectedEntry.icon.explication}</h3>
                        <BasicTimePicker selectedTime={dayjs(selectedEntry?.createdAt) } onTimeChange={handleTimeChange} />
                        <LabelTextareaWhite label="Add comment" onChange={onChange} name="Information" placeholder={selectedEntry.description} type="text" />
                        <button onClick={() => updateEntry(selectedEntry)} className="shadow bg-red-500 hover:bg-red-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded mb-4">Update</button>
                    </div>
                </Modal>
            )
            }
        </>
    )
}

export default EntryList