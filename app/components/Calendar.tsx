'use client'
import * as React from 'react';
import dayjs, { Dayjs } from 'dayjs';
import Badge from '@mui/material/Badge';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { PickersDay, PickersDayProps } from '@mui/x-date-pickers/PickersDay';
import { DateCalendar } from '@mui/x-date-pickers/DateCalendar';

const initialValue = dayjs(new Date());

interface CalendarProps {
    fetchedDates: number[];
    onDayClick: (selectedDay: Dayjs) => void;

}
export default function DateCalendarServerRequest({ fetchedDates, onDayClick }: CalendarProps) {
    const [highlightedDays, setHighlightedDays] = React.useState(fetchedDates);
    const [selectedDay, setSelectedDay] = React.useState<Dayjs | null>(null);

    const handleMonthChange = (selectedDay: Dayjs) => {
        console.log("selected day:", selectedDay);
    };

    const handleDayClick = (day: Dayjs) => {
        setSelectedDay(day);
        onDayClick(day);
    };

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateCalendar
                defaultValue={initialValue}
                onMonthChange={handleMonthChange}
                slots={{
                    day: (props) => <ServerDay {...props} onDayClick={handleDayClick} />,
                }}
                slotProps={{
                    day: {
                        highlightedDays,
                    } as any,
                }}
            />
        </LocalizationProvider>
    );
}



function ServerDay(props: PickersDayProps<Dayjs> & { highlightedDays?: number[], onDayClick: (day: Dayjs) => void }) {
    const { highlightedDays = [], day, outsideCurrentMonth, onDayClick, ...other } = props;

    const isSelected =
        !props.outsideCurrentMonth && highlightedDays.indexOf(props.day.date()) >= 0;
    const handleDayClick = () => {
        onDayClick(day);
    };

    return (
        <Badge
            key={day.toString()}
            overlap="circular"
            badgeContent={isSelected ? '🧸' : undefined}
        >
            <PickersDay
                {...other}
                outsideCurrentMonth={outsideCurrentMonth}
                day={day}
                onClick={handleDayClick}
            />
        </Badge>
    );
}
