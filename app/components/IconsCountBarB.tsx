'use client'
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react'
import { BabyProfile, EntryCount} from '../api/type';
import IconCount from './elements/IconCount';
import { getEntryCountByBabyIdAndDate } from '../api/entryService';

interface ButtonListProps {
    babyProfile:BabyProfile;
    date:string;
}

const IconsCountBarB = ({babyProfile, date}:ButtonListProps) => {
    console.log(babyProfile)
    const [counts,setCounts]=useState<EntryCount[]>([]);
    useEffect(()=>{
        getData()
    },[])
    const getData = async()=>{
        const result = await getEntryCountByBabyIdAndDate(babyProfile.id as number,dayjs(new Date(date)).format('YYYY-MM-DD'))
        setCounts(result);
    }
    return (
        <div className="w-72">
            {counts && counts.length>0?(<div className="w-full flex items-center justify-start gap-3  mt-2">
            {counts?.map((item) => (
                <IconCount
                    key={item.id}
                    entryCount={item} 
                    />
            ))}
            </div>):(
                <div>No entry</div>
            )}
            
        </div>

    )
}

export default IconsCountBarB