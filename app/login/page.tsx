'use client'
import { ChangeEvent, FormEvent, useContext, useState } from "react";
import { useRouter } from "next/navigation";
import { AuthContext } from "../api/auth/AuthContextProvider";
import { login } from "../api/auth/authService";
import LabelForm from "../components/elements/LabelForm";
import Button from "../components/elements/Button";

export default function loginPage() {
    const { setToken } = useContext(AuthContext);
    const [error, setError] = useState('');
    const [loginitem, setLoginitem] = useState({
        email: '',
        password: '',
    });
    const router = useRouter();

    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setLoginitem({
            ...loginitem,
            [event.target.name]: event.target.value
        });
    }
    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        try {
            const data = (await login({email:loginitem.email, password:loginitem.password}) as any);
            setToken(data.token); 
            router.push('/user')
        } catch (error: any) {
            console.log(error);
            if (error.response?.status == 401) {
                setError('Mauvais identifiant ou  mot de passe');

            } else {
                setError('Erreur serveur');
            }
        }
        setLoginitem({
            email: '',
            password: '',
        })
    }

    return (
        <div className="flex flex-col items-center justify-center min-h-screen  ">
            {error && <p className="text-red-500">{error}</p>}
            <div className="">
            <form className="w-full max-w-sm" >
                <LabelForm label="Email" onChange={handleChange} name="email" type="email"/>
                <LabelForm label="Password" onChange={handleChange} name="password" placeholder="******************" type="password"/>
                <div className="flex justify-center gap-4">
                <div className=" flex justify-center ">
                    <Button onClick={handleSubmit} text="Login"/>
                </div>
                <div className=" flex justify-center ">
                    <Button onClick={()=>router.push("/")} text="Retour"/>
                </div>
                </div>
            </form>
            </div>
        </div>
    )
}
