
import { parseCookies } from "nookies";
import { Entry, EntryCount} from "./type";

export const postEntry = async (entry:Entry):Promise<Entry|undefined>=>{
    const { token } = parseCookies();
    try{
    const res = await fetch('http://localhost:8000/api/connect/entry',{
    method:"POST", 
    headers:{
        "Content-Type":"application/json",
        Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(entry)
});
const newEntry = await res.json();
return newEntry;
    }catch(e:any){
        console.log(e)
    } 
}

export const getEntryByBabyId = async (id:number):Promise<Entry[]>=>{
    const { token } = parseCookies();
    const res = await fetch(`http://localhost:8000/api/connect/entry/babyProfile/${id}`,{
        method:"GET",
        headers:{
            Authorization: `Bearer ${token}`
        }
    });
    const resultat = res.json();
    return resultat;
}

export const getEntryByBabyIdAndDate = async (id:number,date:string):Promise<Entry[]|undefined>=>{
    const { token } = parseCookies();
    const res = await fetch(`http://localhost:8000/api/connect/entry/babyProfile/${id}/date/${date}`,{
        method:"GET",
        headers:{
            Authorization: `Bearer ${token}`
        }
    });
    const resultat = res.json();
    return resultat;
}

export const getEntryCountByBabyIdAndDate = async (id:number,date:string):Promise<EntryCount[]|undefined>=>{
    const { token } = parseCookies();
    try{
        const res = await fetch(`http://localhost:8000/api/connect/entry/babyProfile/${id}/date/${date}/entryCount`,{
            method:"GET",
            headers:{
                Authorization: `Bearer ${token}`
            }
        });
        const resultat = res.json();
        return resultat;
    }catch(e){
        console.log(e)
    }
}

export const editEntry = async (entry:Entry):Promise<Entry>=>{
    const { token } = parseCookies();
    const res = await fetch(`http://localhost:8000/api/connect/entry/${entry.id}`,{
    method:"PUT", 
    headers:{
        "Content-Type":"application/json",
        Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(entry)
});
    const updatedEntry= res.json();
    console.log(updatedEntry)
    return updatedEntry;
}

export const deleteEnt = async (entry:Entry)=>{
    const { token } = parseCookies();
    await fetch(`http://localhost:8000/api/connect/entry/${entry.id}`,{
        method:"DELETE",
        headers:{
            Authorization: `Bearer ${token}`
        }
    });
}


