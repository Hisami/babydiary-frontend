
import { parseCookies } from "nookies";
import { User } from "../type";


export const login = async (logininfo:User):Promise<{token:string}>=>{
    const res = await fetch('http://localhost:8000/api/login',{
    method:"POST", 
    headers:{
        "Content-Type":"application/json",
    },
    body: JSON.stringify(logininfo)
});
    const token = await res.json();
    return token
}

export const signin = async (signinfo:User)=>{
    const res = await fetch('http://localhost:8000/api/user',{
    method:"POST", 
    headers:{
        "Content-Type":"application/json",
    },
    body: JSON.stringify(signinfo)
});
    const resultat = res.json();
    return resultat;
}


export const getUser = async ():Promise<User|null>=>{
    const { token } = parseCookies();
    try{
        const res = await fetch('http://localhost:8000/api/protected',{
            method:"GET",
            headers:{
                Authorization: `Bearer ${token}`
            }
        });   
        const user = await res.json();
        return user;
    }catch(err:any){
        console.log(err.response);
        return null;  
    }  
}

export const editUserProfile = async (userProfile:User):Promise<User|undefined>=>{
    const { token } = parseCookies();
    const res = await fetch(`http://localhost:8000/api/connect/user/${userProfile.id}`,{
    method:"PUT", 
    headers:{
        "Content-Type":"application/json",
        Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(userProfile)
});
    const updatedUserProfile= res.json();
    return updatedUserProfile;
}


