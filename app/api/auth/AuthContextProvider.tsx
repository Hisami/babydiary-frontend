'use client'
import { setCookie, destroyCookie, parseCookies } from "nookies";
import { createContext, useEffect, useState } from "react";
import { User } from "../type";
import { getUser } from "./authService";

interface AuthState {
    token?: string | null;
    setToken: (value: string | null) => void;
    user?:User|undefined;
    setUser?:any;
}

export const AuthContext = createContext({} as AuthState);

export const AuthContextProvider = ({ children }: any) => {
    const [token, setToken] = useState<string | null>('');
    const [user, setUser]=useState<User>();
    async function handleSet(value: string | null) {
        if (value) {
            setCookie(null, 'token', value)
        } else {
            destroyCookie(null, 'token');
        }
        setToken(value);
    }

    useEffect(() => {
        setToken(parseCookies().token);
    }, []);

    return (
        <AuthContext.Provider value={{ token, setToken: handleSet,user, setUser:handleSet}}>
            {children}
        </AuthContext.Provider>
    );
    }

    export const editProfile = async (userProfile:User):Promise<User|undefined>=>{
        const { token } = parseCookies();
        const res = await fetch(`http://localhost:8000/api/user/${userProfile.id}`,{
        method:"PUT", 
        headers:{
            "Content-Type":"application/json",
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify(userProfile)
    });
        const updatedUserProfile= res.json();
        return updatedUserProfile;
    }