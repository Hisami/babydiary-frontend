import { parseCookies } from "nookies";
import { Comment } from "./type";

export const postComment = async (comment: Comment): Promise<Comment> => {
    const { token } = parseCookies();
    try {
        const res = await fetch('http://localhost:8000/api/connect/comment', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(comment)
        });
        if (!res.ok) {
            const errorData = await res.json();
            throw new Error(errorData.message);
        }
        const newComment = await res.json();
        return newComment;
    } catch (error) {
        throw new Error("Error occurred");
    }
}


export const getCommentsByBabyIdAndDate = async (id:number,date:string):Promise<Comment[]|undefined>=>{
    const { token } = parseCookies();
    try{
        const res = await fetch(`http://localhost:8000/api/connect/comment/babyProfile/${id}/date/${date}`,{
            method:"GET",
            headers:{
                Authorization: `Bearer ${token}`
            }
        });
        const resultat = res.json();
        return resultat;
    }catch(e){
        console.log(e)
    }
}

export const editComment = async (comment:Comment):Promise<Comment>=>{
    const { token } = parseCookies();
    const res = await fetch(`http://localhost:8000/api/connect/comment/${comment.id}`,{
    method:"PUT", 
    headers:{
        "Content-Type":"application/json",
        Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(comment)
});
    const updatedComment= res.json();
    return updatedComment;
}

export const deleteComment = async (comment:Comment)=>{
    const { token } = parseCookies();
    await fetch(`http://localhost:8000/api/connect/comment/${comment.id}`,{
    method:"DELETE",
    headers:{
        Authorization: `Bearer ${token}`
    }
});
}


