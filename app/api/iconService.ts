import { parseCookies } from "nookies";
import { Icon } from "./type";


export const getAllIcons = async ():Promise<Icon[]>=>{
    const { token } = parseCookies();
    const res = await fetch('http://localhost:8000/api/connect/icon',{
        method:"GET",
        headers:{
            Authorization: `Bearer ${token}`
        }
    });
    const todos= res.json();
    return todos;
}
