export interface User{
    id?:number;
    email:string;
    password:string;
    name?:string;
    imgUrl?:string
}

export interface AvatarImage{
    id:number;
    imgUrl:string;
    alt:string
}

export interface BabyProfile{
    id?:number;
    name?:string;
    birthDate?:string;
    imgUrl?:string;
    description?:string;
    user:User|null;
}

export interface Icon{
    id?:number;
    name:string;
    explication:string;
}

export interface Entry{
    id?:number;
    createdAt?:string;
    description?:string;
    babyProfile:BabyProfile;
    icon:Icon;
}

export interface EntryCount{
    id:number;
    icon_name:string;
    entryCount:number;
}

export interface Comment{
    id?:number;
    createdAt?:string;
    comment?:string;
    imgUrl?:string;
    babyProfile:BabyProfile
}