import { parseCookies } from "nookies";
import { BabyProfile } from "./type";

export const postBabyProfile = async (babyProfileInfo:BabyProfile) => {
    const { token } = parseCookies();
    try {
        const res = await fetch('http://localhost:8000/api/connect/babyProfile', {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`
            },
            body: JSON.stringify(babyProfileInfo)
        });
        const babyProfile = await res.json(); 
        return babyProfile;
    } catch (error) {
        console.error("An error occurred:", error);
        throw error; 
    }
}


export const getBabyProfileByUser = async (id: number): Promise<BabyProfile[]|undefined> => {
    const { token } = parseCookies();
    try {
        const res = await fetch(`http://localhost:8000/api/connect/babyProfile/user/${id}`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        if (res.ok) {
            const babyProfile = await res.json();
            return babyProfile;
        } else {
            const errorData = await res.json();
            throw new Error(errorData.message);
        }
    } catch (error) {
        console.error("An error occurred:", error);
        throw error;
    }  
}

export const editBabyProfile = async (babyProfile:BabyProfile):Promise<BabyProfile|undefined>=>{
    const { token } = parseCookies();
    const res = await fetch(`http://localhost:8000/api/connect/babyProfile/${babyProfile.id}`,{
    method:"PUT", 
    headers:{
        "Content-Type":"application/json",
        Authorization: `Bearer ${token}`
    },
    body: JSON.stringify(babyProfile)
});
    const updatedBabyProfile= res.json();
    return updatedBabyProfile;
}
