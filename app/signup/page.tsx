'use client'
import { ChangeEvent, FormEvent, useContext, useState } from "react";
import { useRouter } from "next/navigation";
import { AuthContext } from "../api/auth/AuthContextProvider";
import LabelForm from "../components/elements/LabelForm";
import Button from "../components/elements/Button";
import { User } from "../api/type";
import { avatarimages } from "../data/avatarimages";
import AvatarButton from "../components/elements/AvatarButton";
import { login, signin } from "../api/auth/authService";
import ExpandableText from "../components/ExpandedText";


export default function signupPage() {
    const [error, setError] = useState('');
    const { setToken } = useContext(AuthContext);
    const [signupItem, setSignupItem] = useState<User>({
        email: '',
        password: '',
        name: '',
        imgUrl: '',
    });
    const [isAgreed, setIsAgreed] = useState(false);
    const router = useRouter();


    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setSignupItem({
            ...signupItem,
            [event.target.name]: event.target.value
        });
    }

    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        if (!isAgreed) {
            setError("Veuillez accepter la politique de protection des données.");
            return;
        }
        try {
            const user = await signin(signupItem);
            if (user) {
                const data = (await login({ email: signupItem.email, password: signupItem.password }) as any);
                setToken(data.token);
                router.push('/user')
            }
        } catch (error: any) {
            console.log(error);
            if (error.response?.status == 401) {
                setError('Incorrect input');
            } else {
                setError('Erreur serveur');
            }
        }
    }

    return (
        <div className="flex flex-col items-center justify-center min-h-screen  ">
            <h1 className="font-bold text-xl md:text-3xl md:mb-8  "> Créér un compte</h1>
            <div className="mx-8">
                <form className="w-full max-w-sm" >
                    <LabelForm label="Email" onChange={handleChange} name="email" type="email" />
                    <LabelForm label="Password" onChange={handleChange} name="password" placeholder="******************" type="password" />
                    <LabelForm label="Name" onChange={handleChange} name="name" placeholder="Enter userName" type="string" />
                    <div>
                        <div className="flex justify-center">
                            <p className="text-gray-500 font-bold">Select your avatar</p>
                        </div>
                        <div className="md:flex md:items-center overflow-x-scroll">
                            {avatarimages.map((avatardata) => (
                                <AvatarButton
                                    onClick={() => { setSignupItem({ ...signupItem, imgUrl: avatardata.imgUrl }) }}
                                    key={avatardata.id}
                                    imgUrl={avatardata.imgUrl}
                                    alt={avatardata.alt}
                                />
                            ))}
                        </div>
                        {error && <p className="text-red-500">{error}</p>}
                    </div>
                    <div>
                    </div>
                    <div className="flex items-center">
                        <input type="checkbox" id="termsAgreement" className="h-6 w-6"  checked={isAgreed} 
                    onChange={() => setIsAgreed(!isAgreed)}/>
                        <label htmlFor="termsAgreement" className="ml-2">
                            J'accepte la politique de protection des données personnelles
                        </label>
                    </div>
                    <div>
                        <ExpandableText text="En cochant cette case, j'accepte que mes données personnelles soient collectées et traitées conformément aux termes et conditions d'utilisation ainsi qu'à la politique de confidentialité. Je comprends que mes données seront utilisées uniquement aux fins spécifiées et ne seront pas partagées avec des tiers sans mon consentement. J'ai le droit d'accéder à mes données, de les modifier et de les supprimer. Je peux retirer mon consentement à tout moment en me référant aux informations de contact fournies.'https://www.cnil.fr/fr/les-droits-pour-maitriser-vos-donnees-personnelles'
                            " maxLength={15} />
                    </div>

                    <div className="mt-4 flex justify-center">
                        <Button onClick={handleSubmit} text="Créer un account" />
                    </div>
                    <div className="mt-4 flex justify-center">
                        <Button onClick={() => router.push("/")} text="Retour" />
                    </div>
                </form>
            </div>
        </div>
    )
}
