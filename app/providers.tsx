"use client";

import { useState, useEffect, ReactNode} from "react";
import { AuthContextProvider } from "./api/auth/AuthContextProvider";

export default function Providers({children}:{children:ReactNode}){
    const [mounted,setMounted]= useState(false);
    useEffect(()=>{
        setMounted(true)
    },[])
    
    if(!mounted){
        return <>{children}</>;
    }
    
    return (
        <AuthContextProvider>
        {children}
        </AuthContextProvider>
    )
}