'use client'
import { getUser } from '@/app/api/auth/authService';
import { getBabyProfileByUser } from '@/app/api/babyProfileService';
import { BabyProfile, User } from '@/app/api/type';
import UpdateBabyProfile from '@/app/components/UpdateBabyProfile';
import UserProfile from '@/app/components/UserProfile';
import { useCalendarState } from '@mui/x-date-pickers/internals';
import React, { useEffect, useState } from 'react';

const page = () => {
  const [ user,setUser ] = useState<User>();
  const [babyProfile,setBabyProfile]= useState<BabyProfile>();
    useEffect(()=>{
        getData();
    },[])
    const getData = async()=>{
        const userData = await getUser();
        setUser(userData as User)
        if(userData){
          const baby = await getBabyProfileByUser(userData?.id as number);
        if(baby){
          const babyWithUserData = { ...baby[0], user: userData } as BabyProfile;
          setBabyProfile(babyWithUserData as BabyProfile);
        }
        }  
    }
  return (
    <div className="flex flex-col justify-center items-center">
      <UserProfile user={user} onUserUpdate={setUser}/>
      <UpdateBabyProfile babyProfile={babyProfile} onBabyUpdate={setBabyProfile} user={user}/>
    </div>
  )
}

export default page