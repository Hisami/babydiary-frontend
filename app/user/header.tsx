'use client'
import Link from 'next/link'
import React, { useContext, useEffect, useState } from 'react'
import { AuthContext } from '../api/auth/AuthContextProvider';
import { getUser } from '../api/auth/authService';
import { User } from '../api/type';

const Header = () => {
    const [ user,setUser ] = useState<User>();
    const { token,setToken } = useContext(AuthContext);
    useEffect(()=>{
        getData();
    },[token])
    const getData = async()=>{
        setUser(await getUser() as User)
    }
    return (
        <div className="bg-cyan-200 p-2 flex gap-5 items-center">
            <Link className="text-sky-600 hover:text-sky-700" href={"/user"}>
                Home
            </Link>
            <Link className="text-sky-600 hover:text-sky-700" href={"/user/records"}>
                Records
            </Link>
            <Link className="text-sky-600 hover:text-sky-700" href={"/user/profile"}>
                Profile
            </Link>
            <div className="ml-auto flex gap-2">
                {user ?( 
                <div className="flex">
                    <div className="flex flex-col justify-center items-center">
                        <img className="inline-block h-8 w-8 md:h-12 md:w-12 rounded-full  " src={user?.imgUrl}/>
                    </div>
                    <div className="flex items-center">
                    <Link href={"/"}>
                    <button className="bg-red-500 text-white px-2 h-8 text-xs rounded-lg " onClick={() => { ;setToken(null);setUser(undefined)}}>
                    Sign Out
                    </button>
                    </Link>
                    </div>
                </div>
                ):null}

            </div>
        </div>
    )
}

export default Header