'use client'

import { getAllIcons } from "../api/iconService";
import IconList from "../components/IconList";
import { getUser } from "../api/auth/authService";
import { getBabyProfileByUser } from "../api/babyProfileService";
import BabyIcon from "../components/BabyIcon";
import React, {  useContext, useEffect, useState } from "react";
import { BabyProfile, Icon, User } from "../api/type";
import dayjs from "dayjs";
import AddDiary from "../components/AddDiary";
import BabyProfileButton from "../components/elements/BabyProfileButton";
import { AuthContext } from "../api/auth/AuthContextProvider";


export default function Home() {
  const { token } = useContext(AuthContext);
  const [icons,setIcons] = useState<Icon[]>();
  const [today,setToday]= useState<string>( dayjs(new Date()).format('YYYY-MM-DD'));
  const [baby,setBaby] =useState<BabyProfile>();
  const [heart,setHeart]=useState<boolean>(false);

  useEffect(()=>{
    getData();
    setHeart(false)
    },[token])

  const getData= async()=>{
    try{ 
      const user = await getUser() as User;
      const babyProfiles = await getBabyProfileByUser(user.id as number);
      if(babyProfiles && babyProfiles.length >0){
        setBaby(babyProfiles[0] as BabyProfile);
      }
      setToday (dayjs(new Date()).format('YYYY-MM-DD'));
      setIcons( await getAllIcons());
  }catch(e){
        
  }
}
  return (
    <React.Fragment>
      {baby?(<div>
      <BabyIcon babyProfile={baby as BabyProfile}/>
      <div className="flex flex-col justify-center items-center">
        <h2 className="font-bold text-2xl">{baby?.name}'s {new Date().toLocaleDateString("fr")}</h2>
      </div>  
        <IconList  babyProfile={baby as BabyProfile} today={today} icons={icons}/>
        <AddDiary babyProfile={baby} today={today}/>
      </div>):(
      <div className="flex flex-col items-center justify-center min-h-screen">
        {heart && <img className="w-12 md:w-24 rotate-45 absolute top-50 md:top-100 right-20 md:right-50"
        src="https://4.bp.blogspot.com/-XWP8gzXWJXw/UP5whVRKQ1I/AAAAAAAAK8Q/To50kdanjTQ/s1600/heart_small.png"/>}
        <img src="https://1.bp.blogspot.com/-AJjw6k7Oh8Q/U57C96pZS2I/AAAAAAAAhfc/y18XLb5rKO4/s400/akachan_omutsu.png" className="w-32 md:w-48 "/>
        <BabyProfileButton onClick={()=>setHeart(true)}/>
      </div>
      )}    
    </React.Fragment>
  )
}
