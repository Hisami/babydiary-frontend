'use client'
import { getUser } from '@/app/api/auth/authService';
import { getBabyProfileByUser } from '@/app/api/babyProfileService';
import { getAllIcons } from '@/app/api/iconService';
import { BabyProfile, Icon, User } from '@/app/api/type';
import AddDiary from '@/app/components/AddDiary';
import BabyIcon from '@/app/components/BabyIcon';
import IconList from '@/app/components/IconList';
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react'

export default  function Page({ params }: { params: { searchDay: string } }) {
    const [icons, setIcons] = useState<Icon[]>();
    const [today, setToday] = useState<string>(dayjs(new Date()).format('YYYY-MM-DD'));
    const [baby, setBaby] = useState<BabyProfile | undefined>();
    const { searchDay } = params;

    useEffect(() => {
        getData();
    }, [])

    const getData = async () => {
        try {
            setIcons(await getAllIcons());
            const user = await getUser() as User;
            const babyProfiles = await getBabyProfileByUser(user.id as number);
            setBaby(babyProfiles[0]);
            setToday(dayjs(new Date(searchDay)).format('YYYY-MM-DD'));
        } catch (e) {
            console.log(e);
        }
    }
    return (
        <div>
            {baby ? (
                <div>
                    <BabyIcon babyProfile={baby as BabyProfile} date={today} />
                    <div className="flex flex-col justify-center items-center">
                        <h2 className="font-bold text-2xl">{baby?.name}'s {new Date(today).toLocaleDateString("fr")}</h2>
                    </div>
                    <IconList icons={icons as Icon[]} babyProfile={baby as BabyProfile} today={today} />
                    <AddDiary babyProfile={baby} today={today} />
                </div>
            ) : (
                <>
                    <p>No baby profile</p>
                </>
            )}
        </div>
    )
}


