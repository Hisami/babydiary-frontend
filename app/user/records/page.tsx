'use client'
import { getUser } from '@/app/api/auth/authService';
import { getBabyProfileByUser } from '@/app/api/babyProfileService';
import { getEntryByBabyIdAndDate, getEntryCountByBabyIdAndDate } from '@/app/api/entryService';
import { BabyProfile, Entry, EntryCount, User,Comment } from '@/app/api/type';
import BabyIconHorizontal from '@/app/components/BabyIconHorizontal';
import DateCalendarServerRequest from '@/app/components/Calendar'
import IconsCountBar from '@/app/components/IconsCountBar';
import dayjs, { Dayjs } from 'dayjs';
import React, { useEffect, useState } from 'react'
import { useRouter } from "next/navigation";
import { getCommentsByBabyIdAndDate } from '@/app/api/commentService';
import CommentCardList from '@/app/components/CommentCardList';

const recordspage = () => {
  const router = useRouter();
  const [baby, setBaby] = useState<BabyProfile>();
  const [searchDay, setSearchDay] = useState<string>(dayjs(new Date()).format('YYYY-MM-DD') as string);
  const [entryCounts, setEntryCounts] = useState<EntryCount[]>([]);
  const [entries, setEntries] = useState<Entry[]>([]);
  const [comments, setComments] = useState<Comment[]>([]);

  useEffect(() => {
    getData();
  }, []);
  const getData = async () => {
    try {
      const user = await getUser() as User;
      const babyProfiles = await getBabyProfileByUser(user.id as number);
      if(babyProfiles && babyProfiles.length >0){
        setBaby(babyProfiles[0] as BabyProfile)
        setEntryCounts(await getEntryCountByBabyIdAndDate(babyProfiles[0].id as number, dayjs(new Date()).format('YYYY-MM-DD') ) as EntryCount[])
        setEntries(await getEntryByBabyIdAndDate(babyProfiles[0].id as number, dayjs(new Date()).format('YYYY-MM-DD')) as Entry[])
        setComments(await getCommentsByBabyIdAndDate(babyProfiles[0].id as number,dayjs(new Date()).format('YYYY-MM-DD')) as Comment[])
      }
    } catch (e) {
      console.log(e)
    }
  }
  const handleDataClick = async (selectedDay: Dayjs) => {
    const createdDatetime = dayjs(selectedDay).format('YYYY-MM-DD');
    setSearchDay(dayjs(new Date(createdDatetime)).format('YYYY-MM-DD') as string)
    if (baby) {
      try {
        setEntryCounts(await getEntryCountByBabyIdAndDate(baby?.id as number, dayjs(new Date(createdDatetime)).format('YYYY-MM-DD')) as EntryCount[]);
        setEntries(await getEntryByBabyIdAndDate(baby?.id as number, dayjs(new Date(createdDatetime)).format('YYYY-MM-DD')) as Entry[]);
        setComments(await getCommentsByBabyIdAndDate(baby?.id as number,dayjs(new Date(createdDatetime)).format('YYYY-MM-DD')) as Comment[])
      } catch (e) {
        setEntryCounts([]);
        setEntries([]);
        setComments([])
      }
    }
  }
  return (
    <div>
      <DateCalendarServerRequest fetchedDates={[]} onDayClick={handleDataClick} />
      <div className="bg-white border border-gray-200 mdm-16 md:mx-16 rounded-lg">
        <div className="font-bold flex justify-center md:text-2xl">{searchDay}</div>
        <div className="flex flex-col">
          <BabyIconHorizontal babyProfile={baby as BabyProfile} date={searchDay} />
          <div className="flex justify-center">
            {entryCounts && entryCounts.length > 0 ?
              <IconsCountBar entryCounts={entryCounts} /> :
              <p className="m-2">No Entry</p>
            }
          </div>
          <div className='flex justify-center items-center'>
            <div className="bg-white rounded-md border md:w-3/5"　>
              <div className="h-12 bg-orange-400 px-4 flex items-center justify-center text-white">Today's Records</div>
              <div className="px-8 py-6 md:h-88 overflow-scroll">
                <table className="">
                  <thead>
                    <tr>
                      <th className="border-b-2 border-dotted">Time</th>
                      <th className="border-b-2 border-dotted">Event</th>
                      <th className="border-b-2 border-dotted">Comment</th>
                    </tr>
                  </thead>
                  <tbody>
                    {entries && entries.length > 0 ? (
                      <>
                        {entries?.map((entry) => (
                          <tr key={entry.id}>
                            <td className="border-b border-dotted">{dayjs(entry.createdAt as string).format('HH:mm')}</td>
                            <td className="w-24 border-b border-dotted">{entry.icon.name} <span className="size-sm">{entry.icon.explication}</span></td>
                            <td className="border-b border-dotted">
                              <div className="md:ml-8 ml-4">
                                <p>{entry.description}</p>
                              </div>
                            </td>
                          </tr>
                        ))}
                      </>
                    ) : (
                      <tr>
                        <td colSpan={3}>No entries at the moment. Click the icon button to add time and comments.</td>
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <CommentCardList comments={comments?comments:null} isModifiable={false}/>
        <div className="flex justify-center mt-4">
          <button onClick={() => router.push(`/user/records/${searchDay}`)} className="shadow bg-green-500 hover:bg-green-400 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded mb-4">Edit Content</button>
        </div>
      </div>
    </div>
  )
}

export default recordspage