
import React from 'react'
import '../globals.css'
import Header from './header'


export default function UserLayout({children}: {children: React.ReactNode}) {
  return (
      <React.Fragment>
          <Header/>
          {children}  
      </React.Fragment> 
  )
}
