'use client'
import { ChangeEvent, FormEvent, useContext, useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import LabelForm from "../../components/elements/LabelForm";
import Button from "../../components/elements/Button";
import { BabyProfile} from "../../api/type";
import { babyavatarimages } from "../../data/babyavatarimages";
import AvatarButton from "../../components/elements/AvatarButton";
import LabelTextarea from "../../components/elements/LabelTextarea";
import { postBabyProfile } from "../../api/babyProfileService";
import { getUser } from "../../api/auth/authService";
import { AuthContext } from "../../api/auth/AuthContextProvider";
import BasicDatePicker from "../../components/BasicDatePicker";
import dayjs from "dayjs";

export default function babyprofilePage() {
    const { token } = useContext(AuthContext);
    const [error, setError] = useState('');
    const [selectedDate, setSelectedDate] = useState<any>(dayjs(new Date())as any);
    const [babyProfile, setBabyProfile] = useState<BabyProfile>({
        name: '',
        birthDate:'',
        description:'',
        imgUrl: '',
        user:null
    });
    const router = useRouter();

    useEffect(()=>{
        if(token){
            console.log(token)
            fetchUserData().then((user=>setBabyProfile({...babyProfile, user:user}) ))
        }else{   
        }
    },[]);

    async function fetchUserData(){
        const response = await getUser();
        console.log(response)
        return response
    }

    const handleDateChange = (newDate:Date|null ) => {
        setSelectedDate(newDate)
        const createdDatetime = dayjs(newDate).format('YYYY-MM-DD');
        setBabyProfile({...babyProfile,birthDate:createdDatetime});
    };

    function handleChange(event: ChangeEvent<HTMLInputElement>) {
        setBabyProfile({
            ...babyProfile,
            [event.target.name]: event.target.value
        });
    }
    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        console.log("baby",babyProfile)
        try {
            await postBabyProfile(babyProfile);
            router.refresh();
            router.push('/user')
        } catch (error: any) {
            console.log(error);
            if (error.response?.status == 401) {
                setError('Incorrect input');
            } else {
                setError('Erreur serveur');
            }
        }
    }

    return (
        <div className="flex flex-col items-center justify-center min-h-screen  ">
            <h1 className="font-bold text-xl md:text-3xl  "> Créér Baby Profile</h1>
            {error && <p className="text-red-500">{error}</p>}
            <div className="mx-8 md:mt-8">
                <form className="w-full max-w-sm" >
                <LabelForm label="Name" onChange={handleChange} name="name" placeholder="Enter Baby's Name" type="string" />
                <LabelTextarea label="Description" onChange={handleChange} name="description" placeholder="About this baby" type="string"/>
                <p className="text-gray-500 font-bold">Date de naissance</p>
                <BasicDatePicker selectedDate={selectedDate} onDateChange={handleDateChange} />
                <div> 
                    <div className="md:flex md:justify-center mt-6">
                        <p className="text-gray-500 font-bold">Select your avatar</p>
                    </div>
                <div className="md:flex md:items-center overflow-x-scroll">               
                {babyavatarimages.map((avatardata) => (
                    <AvatarButton
                        onClick={()=> {setBabyProfile({...babyProfile,imgUrl:avatardata.imgUrl})}}
                        key={avatardata.id}
                        imgUrl={avatardata.imgUrl}
                        alt={avatardata.alt}
                    />
                ))}
                    </div>
                </div>  
                <div>
                    </div>
                        <div className=" flex justify-center mt-6">
                            <Button onClick={handleSubmit} text="Submit un baby" />
                        </div>
                </form>
            </div>
        </div>
    )
}